package br.com.test.doido.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.test.doido.model.Release;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
@EnableAutoConfiguration
public class ApplicationConfig {

	@RequestMapping("/arroz")
	@ResponseBody
        Release home() {
		Release release = new Release();
		release.setVersion("1.7.5");
		return release;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ApplicationConfig.class, args);
	}
	
}
